angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicPopover) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

   $ionicPopover.fromTemplateUrl('templates/my-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

   $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
})

.controller('loginCtrl', function($scope, $state, $rootScope, popup) {
 $scope.loginData = {};

  $scope.doLogin = function() {
        if ($scope.loginData.email == null || $scope.loginData.senha == null) {
            popup("Opsss", "Parece que você esqueceu de algo.");
            return;
        } else {
           if($scope.loginData.email == "usuario@dirz.com" && $scope.loginData.senha == "12345"){
              $rootScope.dataLogin = $scope.loginData;
              $state.go('app.questionarios');
            }else
              popup("Opsss", "Login ou senha incorreto.");
        }
    }
})
.controller('QuestionariosCtrl', function($scope) {
  $scope.questionarios = [
    {title: 'Direito Penal', id: 1}
  ];
})

.controller('LoginCtrl', function($scope){
  // Form data for the login modal
  $scope.loginData = {};

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('QuestionarioCtrl', function($scope, $stateParams, $http, $ionicModal) {
  $scope.questionario = $stateParams.id;

  $scope.questoes =  [];
  $scope.questaoatual = {};
  $scope.finalizado = false;
  $detalhaAtual = {};

  $http
    .get('templates/questionarios/'+$stateParams.id+'.json')
      .success(function(response) {
        $scope.questoes = response;
        $scope.questaoatual = $scope.questoes[0];
      }
    );

      $scope.previousQuestion = function(){
        var atual = $scope.questaoatual.id;

        if(atual == 1)
        {
          console.log("erro, na da mais pr voltar lesado");
          return;
        }
        $scope.questaoatual = $scope.questoes[(parseInt(atual)-1)-1];

      }
   $scope.nextQuestion = function(){
        var atual = $scope.questaoatual.id;

        $scope.questoes[(atual-1)].respostaEscolhida = $scope.questaoatual.respostaEscolhida;
          console.log($scope.questoes);
        if(atual == $scope.questoes.length)
        {
          return;
        }
        $scope.questaoatual = $scope.questoes[(parseInt(atual)+1)-1];

      }

    $scope.corrigirQuestoes = function(){
        $scope.contaQuestoesCorretas = 0;

        for(i = 0; i<$scope.questoes.length; i++){
            if($scope.questoes[i].respostaEscolhida == $scope.questoes[i].correctId){
                $scope.contaQuestoesCorretas++;
            }

        }
        $scope.total = $scope.questoes.length;
        $scope.finalizado = true;
        $scope.porcentagem = $scope.contaQuestoesCorretas*100/$scope.questoes.length;
        console.log("quantidade de questoes acertadas:"+$scope.contaQuestoesCorretas);
        console.log("porcentagem:"+porcentagem);

    }

    $scope.detalhaQuestao = function(id){
        $scope.detalhaAtual = $scope.questoes[(id-1)];
        $scope.showQuestao = true;
    }

    $scope.setColor = function(marcada, certa){
      if (marcada==certa) {
        return "button-balanced";
      }else{
        return "button-assertive";
      }
    }
    $scope.setCor = function(opcao, certa){
      if (opcao==certa) {
        return "item-balanced";
      }else{
        return "item-assertive";
      }
    }

    $scope.converteIdAlternativa = function(id){
      switch(id) {
        case "0":
          return "A";
          break;
        case "1":
          return "B";
          break;
        
        case "2":
          return "C";
        break;
        
        case "3":
          return "D";
        break;

        case "4":
          return "E";
        break;
      }

    }

   $ionicModal.fromTemplateUrl('my-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };
});
