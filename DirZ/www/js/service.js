angular.module('dirz.services', [])

.constant('dirz', {
        "url": "api-link"
    })
    .factory('requisicao', function($http, dirz) {

        return function(url, success, error, method, params) {

            $http({
                    'url': dirz.url + url,
                    'method': method,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    transformRequest: function(obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    'data': params
                })
                .then(
                    function successCallback(response) {
                        if (typeof(success) == "function")
                            success(response);
                    },
                    function errorCallback(response) {
                        if (typeof(error) == "function")
                            error(response);
                    }
                );
        }
    })
    .factory('popup', function($ionicPopup) {
        return function(titulo, msg) {
            $ionicPopup.alert({
                title: titulo,
                template: msg,
                okText: 'Prosseguir'
            });
        }
    })
    .factory('api', function($http, requisicao) {


        return {
            login: function(loginData, success, error) { //login service
                requisicao('/api/auth', success, error, "POST", loginData);
            }
        };
    });